export default {
    input: 'dist/index.js',
    output: {
      file: 'dist/bundles/breadcrumb.umd.js',
      format: 'umd',
      name: 'ng.breadcrumb',
      sourcemap: false
    },
    globals: {
      '@angular/core': 'ng.core',
      'rxjs': 'Rx',
      'rxjs/operators': 'Rx.Observable.prototype'
    },
  }
